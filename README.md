##MACHINE LEARNING | IMPLEMENTATION AND TESTING OF THE DECISION TREE LEARNING ALGORITHM

##Project Description

Code Language 	: Python

Libraries Used	: Pandas

�	Constructed the decision tree using Information heuristic and Variance impurity heuristic without using the inbuilt decision tree libraries. 

�	Improved the accuracy results using reduced error post pruning algorithm on the tree.
