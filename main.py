import math, operator
import pandas as pd


class CreateTreeNode:
    def __init__(self, attr):
        self.node_value = attr
        self.node_zero = None
        self.node_one = None


class CreateTree:
    attributes_common_class = {}

    def __init__(self):
        self.root_node = None

    def algorithm(self, data_set, attributes, select):
        counts = data_set['Class'].value_counts().to_dict()

        if len(attributes) != 0 and len(counts) == 1:
            if self.get_max_key(counts) == 0:
                root_node = CreateTreeNode(0)

            if self.get_max_key(counts) == 1:
                root_node = CreateTreeNode(1)

        if len(attributes) == 0:
            label = self.most_common_target_value(data_set)
            root_node = CreateTreeNode(label)

        if len(attributes) != 0 and len(counts) == 2:
            if select == 1:
                best_attribute = self.attribute_selection_variance(data_set, attributes)

            if select == 0:
                best_attribute_list = {attr: self.information_gain(data_set, attr) for attr in attributes}
                best_attribute = self.get_max_key(best_attribute_list)

            self.attributes_common_class[best_attribute] = self.most_common_target_value(data_set)
            attributes.remove(best_attribute)
            root_node = CreateTreeNode(best_attribute)
            for value in data_set[best_attribute].value_counts().to_dict():
                data_sub = data_set.loc[data_set[best_attribute] == value]
                if value == 1:
                    if len(data_sub) == 0:
                        root_node.node_one = CreateTreeNode(self.most_common_target_value(data_set))
                    else:
                        root_node.node_one = self.algorithm(data_sub, attributes, select)
                if value == 0:
                    if len(data_sub) == 0:
                        root_node.node_zero = CreateTreeNode(self.most_common_target_value(data_set))
                    else:
                        root_node.node_zero = self.algorithm(data_sub, attributes, select)
        return root_node

    def most_common_target_value(self, df):
        counts = df['Class'].value_counts().to_dict()
        return self.get_max_key(counts)

    def print_t(self, node, level):
        if node is not None:
            if isinstance(node.node_value, str):
                i = 0
                print('\n', ' | '*level, node.node_value, ' = ', i, ': ', end='')
                if node.node_zero is not None:
                    if isinstance(node.node_zero.node_value, int):
                        self.print_t(node.node_zero, level)
                    else:
                        self.print_t(node.node_zero, level+1)
                i = 1
                print('\n', ' | '*level, node.node_value, ' = ', i, ': ', end='')
                if node.node_one is not None:
                    if isinstance(node.node_one.node_value, int):
                        self.print_t(node.node_one, level)
                    else:
                        self.print_t(node.node_one, level+1)
            else:
                print(node.node_value, end='')

    def attribute_selection_variance(self, df, attribute_values):
        total_set = df.shape[0]
        counts_k0 = df['Class'].tolist().count(0)
        counts_k1 = df['Class'].tolist().count(1)

        if counts_k0 == 0 or counts_k1 == 0:
            vi = 0
        else:
            vi = (counts_k0*counts_k1)/(total_set*total_set)

        vi_values = dict()

        for attr in attribute_values:
            count_0 = df[attr].tolist().count(0)
            count_1 = df[attr].tolist().count(1)
            counts_0_0 = len(df[(df[attr] == 0) & (df['Class'] == 0)])
            counts_0_1 = len(df[(df[attr] == 0) & (df['Class'] == 1)])
            counts_1_0 = len(df[(df[attr] == 1) & (df['Class'] == 0)])
            counts_1_1 = len(df[(df[attr] == 1) & (df['Class'] == 1)])

            if counts_0_0 == 0 or counts_0_1 == 0:
                vi_0 = 0
            else:
                vi_0 = (counts_0_0 * counts_0_1) / (count_0 * count_0)

            if counts_1_0 == 0 or counts_1_1 == 0:
                vi_1 = 0
            else:
                vi_1 = (counts_1_0 * counts_1_1) / (count_1 * count_1)

            vi_values[attr] = vi - (count_0/total_set) * vi_0 + (count_1/total_set) * vi_1
        return max(vi_values.items(), key=operator.itemgetter(1))[0]

    def entropy_info(self, data):
        total_set = 0.0
        val_freq = data['Class'].value_counts().to_dict()
        for freq in val_freq.values():
            total_set += (-freq / len(data)) * math.log(freq / len(data), 2)
        return total_set

    def information_gain(self, data, attr):
        sub = 0.0
        val_freq = data[attr].value_counts().to_dict()
        for val in val_freq:
            val_prob = val_freq[val] / sum(val_freq.values())
            data_subset = data.loc[data[attr] == val]
            sub += val_prob * self.entropy_info(data_subset)
        return self.entropy_info(data) - sub

    def get_max_key(self, d1):
        return max(d1.items(), key=operator.itemgetter(1))[0]

    def accuracy_calculator(self, node, dataf, success_c):
        if dataf.shape[0] == 0:
            return success_c
        else:
            node_d = node
            while node_d is not None and isinstance(node_d.node_value, str):
                d = node_d.node_value
                val = dataf.iloc[0][d]
                if val == 0:
                    node_d = node_d.node_zero
                if val == 1:
                    node_d = node_d.node_one
            if node_d is not None:
                if dataf.iloc[0]['Class'] == node_d.node_value:
                    success_c += 1
            dataf1 = dataf.iloc[1:]
            return self.accuracy_calculator(node, dataf1, success_c)

    def apply_accuracies(self, main_node, node, validate_df, acc_dict):
        if node is not None and isinstance(node.node_value, str):
            temp = node.node_value
            node.node_value = self.attributes_common_class[node.node_value]
            accuracy_value = self.accuracy_calculator(main_node, validate_df, 0) / validate_df.shape[0]
            node.node_accuracy = accuracy_value
            node.node_value = temp
            acc_dict[node.node_value] = accuracy_value
            self.apply_accuracies(main_node, node.node_one, validate_df, acc_dict)
            self.apply_accuracies(main_node, node.node_zero, validate_df, acc_dict)

    def help_acc(self, main_node, validate_df, acc_dict):
        node = main_node
        if node is not None:
            if isinstance(node.node_value, str):
                self.apply_accuracies(main_node, main_node.node_one, validate_df, acc_dict)
                self.apply_accuracies(main_node, main_node.node_zero, validate_df, acc_dict)

    def s_d(self, tnode, common_cl, valuz, del_valuz):
        if isinstance(tnode.node_value, str):
            if del_valuz[tnode.node_value] == valuz:
                tnode.node_value = common_cl[tnode.node_value]
                return
            else:
                self.s_d(tnode.node_zero, common_cl, valuz, del_valuz)
                self.s_d(tnode.node_one, common_cl, valuz, del_valuz)
        else:
            return

    def help_search_delete(self, node, common_c, del_accuracy, del_dict):
        if node is not None:
            if isinstance(node.node_value, str):
                self.s_d(node.node_zero, common_c, del_accuracy, del_dict)
                self.s_d(node.node_one, common_c, del_accuracy, del_dict)

    def prune_tree(self, main_node, validate_df, c_class, given_accuracy):
        compare_acc = given_accuracy
        while True:
            if isinstance(main_node.node_one.node_value, int) and isinstance(main_node.node_zero.node_value, int):
                return
            a_dict = dict()
            self.help_acc(main_node, validate_df, a_dict)
            v = self.get_max_key(a_dict)
            max_node = a_dict[v]
            if compare_acc > max_node:
                return
            else:
                self.help_search_delete(main_node, c_class, a_dict[v], a_dict)
                compare_acc = a_dict[v]


df21 = pd.read_csv('training_set1.csv.txt')
df22 = pd.read_csv('validation_set1.csv.txt')
df23 = pd.read_csv('test_set1.csv.txt')


attributes_list11 = df21.columns.tolist()
attributes_list11.remove('Class')

ct0 = CreateTree()
ct0.root_node = ct0.algorithm(df21, attributes_list11, 0)
ct0.print_t(ct0.root_node, 0)

success_accuracy111 = ct0.accuracy_calculator(ct0.root_node, df21, 0)
prune_accuracy111 = (success_accuracy111/df21.shape[0])
print('\n', prune_accuracy111)

success_accuracy112 = ct0.accuracy_calculator(ct0.root_node, df22, 0)
prune_accuracy112 = (success_accuracy112/df22.shape[0])
print('\n', prune_accuracy112)

success_accuracy113 = ct0.accuracy_calculator(ct0.root_node, df23, 0)
prune_accuracy113 = (success_accuracy113/df23.shape[0])
print('\n', prune_accuracy113)

ct0.prune_tree(ct0.root_node, df22, ct0.attributes_common_class, prune_accuracy112)
ct0.print_t(ct0.root_node, 0)

success_accuracy121 = ct0.accuracy_calculator(ct0.root_node, df21, 0)
prune_accuracy121 = (success_accuracy121/df21.shape[0])
print('\n', prune_accuracy121)

success_accuracy122 = ct0.accuracy_calculator(ct0.root_node, df22, 0)
prune_accuracy122 = (success_accuracy122/df22.shape[0])
print('\n', prune_accuracy122)

success_accuracy123 = ct0.accuracy_calculator(ct0.root_node, df23, 0)
prune_accuracy123 = (success_accuracy123/df23.shape[0])
print('\n', prune_accuracy123)

attributes_list12 = df21.columns.tolist()
attributes_list12.remove('Class')

ct1 = CreateTree()
ct1.root_node = ct1.algorithm(df21, attributes_list12, 1)
ct1.print_t(ct1.root_node, 0)

success_accuracy211 = ct1.accuracy_calculator(ct1.root_node, df21, 0)
prune_accuracy211 = (success_accuracy211/df21.shape[0])
print('\n', prune_accuracy211)

success_accuracy212 = ct1.accuracy_calculator(ct1.root_node, df22, 0)
prune_accuracy212 = (success_accuracy212/df22.shape[0])
print('\n', prune_accuracy212)

success_accuracy213 = ct1.accuracy_calculator(ct1.root_node, df23, 0)
prune_accuracy213 = (success_accuracy213/df23.shape[0])
print('\n', prune_accuracy213)

ct1.prune_tree(ct1.root_node, df22, ct1.attributes_common_class, prune_accuracy212)
ct1.print_t(ct1.root_node, 0)

success_accuracy221 = ct1.accuracy_calculator(ct1.root_node, df21, 0)
prune_accuracy221 = (success_accuracy221/df21.shape[0])
print('\n', prune_accuracy221)

success_accuracy222 = ct1.accuracy_calculator(ct1.root_node, df22, 0)
prune_accuracy222 = (success_accuracy222/df22.shape[0])
print('\n', prune_accuracy222)

success_accuracy223 = ct1.accuracy_calculator(ct1.root_node, df23, 0)
prune_accuracy223 = (success_accuracy223/df23.shape[0])
print('\n', prune_accuracy223)
